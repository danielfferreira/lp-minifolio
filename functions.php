<?php

function load_scripts() {
  wp_enqueue_style('bootstrap-min', trailingslashit( get_template_directory_uri() ) . 'css/bootstrap.min.css');
  wp_enqueue_style('fancybox', trailingslashit( get_template_directory_uri() ) . 'css/jquery.fancybox.css');
  wp_enqueue_style('main', trailingslashit( get_template_directory_uri() ) . 'css/main.css');
  wp_enqueue_style('responsive', trailingslashit( get_template_directory_uri() ) . 'css/responsive.css');
  wp_enqueue_style('animate-min', trailingslashit( get_template_directory_uri() ) . 'css/animate.min.css');
  wp_enqueue_style('font-awesome-min', trailingslashit( get_template_directory_uri() ) . 'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css');

  wp_register_script('jquery-two', 'https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js', array(), '1.11.3', true);

  wp_enqueue_script('bootstrap-min', trailingslashit( get_template_directory_uri() ) . 'js/bootstrap.min.js', array('jquery-two'), null, true);
  wp_enqueue_script('fancybox', trailingslashit( get_template_directory_uri() ) . 'js/jquery.fancybox.pack.js', array('jquery-two'), null, true);
  wp_enqueue_script('waypoints-min', trailingslashit( get_template_directory_uri() ) . 'js/jquery.waypoints.min.js', array('jquery-two'), null, true);
  wp_enqueue_script('retina-min', trailingslashit( get_template_directory_uri() ) . 'js/retina.min.js', array('jquery-two'), null, true);
  wp_enqueue_script('modernizr', trailingslashit( get_template_directory_uri() ) . 'js/modernizr.js', array('jquery-two'), null, true);
  wp_enqueue_script('main', trailingslashit( get_template_directory_uri() ) . 'js/main.js', array('jquery-two'), null, true);
}

add_action('wp_enqueue_scripts', 'load_scripts');


function minifolio_sidebars() {
  register_sidebar(
    array(
      'name'=>'Banner',
      'id'=>'banner',
      'description'=>'type any text here', 
      'before_widget'=> '<div class="wrap">',
      'after_widget'=>'</div>',
      'before_title'=> '<h2 class="widget-title">', 
      'after_title'=>'</h2>'
    )
  );
  register_sidebar(
    array(
      'name' => 'About Me',
      'id' => 'about-me',
      'description' => 'Type any text here too!',
      'before_widget' => '<div class="widget-wrapper">',
      'after_widget' => '</div>',
      'before_title' => '<h2 class="widget-title">',
      'after_title' => '</h2>'
    )
  );
  
  register_sidebar(
    array(
      'name' => 'Hire Me',
      'id' => 'hire-me',
      'description' => 'Please, drag your widgets here',
      'before_widget' => '<div class="widget-wrapper">',
      'after_widget' => '</div>',
      'before_title' => '<h2 class="widget-title">',
      'after_title' => '</h2>'
    )
  );
  register_sidebar(
    array(
      'name' => 'Contact',
      'id' => 'contact',
      'description' => 'Please, drag your widgets here',
      'before_widget' => '<div class="widget-wrapper">',
      'after_widget' => '</div>',
      'before_title' => '<h2 class="widget-title">',
      'after_title' => '</h2>'
    )
  );
}

register_nav_menus(
  array(
    /* 
      chave => valor
      posição => descrição 
    */
    'primary' => 'Primary Menu',
    'social' => 'Social Menu'
  )
);

add_action('widgets_init', 'minifolio_sidebars');

add_theme_support('post-thumbnails');
add_theme_support('custom-logo');

